defmodule Biddy.Accounts.User do
  @moduledoc """
  The User model
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Argon2

  schema "users" do
    field :avatar, :string
    field :display_name, :string
    field :email, :string
    field :favorites, {:array, :string}
    field :password_hash, :string

    ## Virtual Fields ##
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    ## Associations ##
    has_many :items, Biddy.Auction.Item
    has_many :bids, Biddy.Auction.Bid
    timestamps()
  end

  @required_fields ~w(display_name password email)a
  @optional_fields ~w(avatar favorites)a

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/(\w+)@([\w.]+)/)
    |> unique_constraint(:email)
    |> validate_length(:display_name, min: 3)
    |> validate_length(:password, min: 6)
    |> put_password_hash()
  end

  defp put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: pass}} = changeset) do
    change(changeset, password_hash: Argon2.hash_pwd_salt(pass))
  end

  defp put_password_hash(changeset), do: changeset
end
