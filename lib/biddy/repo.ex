defmodule Biddy.Repo do
  use Ecto.Repo,
    otp_app: :biddy,
    adapter: Ecto.Adapters.Postgres
end
