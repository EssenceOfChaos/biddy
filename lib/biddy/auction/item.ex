defmodule Biddy.Auction.Item do
  @moduledoc """
  The Item model
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "items" do
    field :description, :string
    field :ends_at, :utc_datetime
    field :listed_at, :utc_datetime
    field :title, :string
    ## Associations ##
    has_many :bids, Biddy.Auction.Bid
    belongs_to :user, Biddy.Accounts.User
    timestamps()
  end

  @required_fields ~w(title description)a
  @optional_fields ~w(listed_at ends_at)a

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:title, min: 3)
    |> validate_length(:description, max: 200)
    |> validate_change(:ends_at, &validate/2)
  end

  defp validate(:ends_at, ends_at_date) do
    case DateTime.compare(ends_at_date, DateTime.utc_now()) do
      :lt -> [ends_at: "ends_at cannot be in the past"]
      _ -> []
    end
  end
end
