defmodule Biddy.Auction.Bid do
  @moduledoc """
  The Bid model
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "bids" do
    field :amount, :integer
    ## Associations ##
    belongs_to :item, Biddy.Auction.Item
    belongs_to :user, Biddy.Accounts.User
    timestamps()
  end

  @doc false
  def changeset(bid, attrs \\ %{}) do
    bid
    |> cast(attrs, [:amount, :user_id, :item_id])
    |> validate_required([:amount, :user_id, :item_id])
    |> assoc_constraint(:item)
    |> assoc_constraint(:user)
  end
end
