defmodule BiddyWeb.ItemController do
  use BiddyWeb, :controller

  alias Biddy.Auction
  alias Biddy.Auction.Item

  def index(conn, _params) do
    items = Auction.list_items()
    render(conn, "index.html", items: items)
  end

  def new(conn, _params) do
    changeset = Auction.change_item(%Item{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"item" => item_params}) do
    user = Guardian.Plug.current_resource(conn)

    case Auction.create_item(user, item_params) do
      {:ok, item} ->
        conn
        |> put_flash(:info, "\"#{item.title}\" was created successfully.")
        |> redirect(to: Routes.item_path(conn, :show, item))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  ###########################################
  def show(conn, %{"id" => id}) do
    item = Auction.get_item_with_bids(id)
    bid = Auction.new_bid()
    render(conn, "show.html", item: item, bid: bid)
  end

  ###########################################

  def edit(conn, %{"id" => id}) do
    item = Auction.get_item!(id)
    changeset = Auction.change_item(item)
    render(conn, "edit.html", item: item, changeset: changeset)
  end

  def update(conn, %{"id" => id, "item" => item_params}) do
    item = Auction.get_item!(id)

    case Auction.update_item(item, item_params) do
      {:ok, item} ->
        conn
        |> put_flash(:info, "Item updated successfully.")
        |> redirect(to: Routes.item_path(conn, :show, item))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", item: item, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    item = Auction.get_item!(id)
    {:ok, _item} = Auction.delete_item(item)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: Routes.item_path(conn, :index))
  end
end
