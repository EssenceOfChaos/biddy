defmodule BiddyWeb.SessionController do
  use BiddyWeb, :controller
  alias Biddy.{Accounts, Accounts.Guardian}

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"user" => %{"email" => email, "password" => password}}) do
    case Accounts.authenticate_user(email, password) do
      {:ok, user} ->
        conn
        |> Guardian.Plug.sign_in(user)
        |> assign(:user_signed_in?, true)
        # |> configure_session(renew: true)
        |> put_flash(:info, "Successfully authenticated as " <> user.email <> ".")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, _} ->
        conn
        |> put_flash(:error, "Invalid email or password")
        |> render("new.html")
    end
  end

  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    Accounts.authenticate_user(email, password)
    |> login_reply(conn)
  end

  def logout(conn, _) do
    conn
    |> put_flash(:info, "You've been successfully logged out")
    |> Guardian.Plug.sign_out()
    |> redirect(to: Routes.page_path(conn, :index))
  end

  defp login_reply({:ok, user}, conn) do
    conn
    |> put_flash(:info, "Welcome back! " <> user.email)
    |> Guardian.Plug.sign_in(user)
    |> redirect(to: "/protected")
  end

  defp login_reply({:error, reason}, conn) do
    conn
    |> put_flash(:error, to_string(reason))
    |> new(%{})
  end

  def delete(conn, _) do
    conn
    |> Accounts.logout()
    |> put_flash(:info, "You've been successfully logged out")
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
