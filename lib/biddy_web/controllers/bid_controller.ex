defmodule BiddyWeb.BidController do
  use BiddyWeb, :controller

  alias Biddy.Auction
  # alias Biddy.Auction.Bid

  def create(conn, %{"bid" => %{"amount" => amount}, "item_id" => item_id}) do
    user = Guardian.Plug.current_resource(conn)

    case Auction.insert_bid(%{amount: amount, item_id: item_id, user_id: user.id}) do
      {:ok, bid} ->
        html =
          Phoenix.View.render_to_string(
            BiddyWeb.BidView,
            "bid.html",
            bid: bid,
            username: user.display_name
          )

        BiddyWeb.Endpoint.broadcast("item:#{item_id}", "new_bid", %{body: html})
        redirect(conn, to: Routes.item_path(conn, :show, bid.item_id))

      {:error, bid} ->
        item = Auction.get_item(item_id)
        render(conn, BiddyWeb.ItemView, "show.html", item: item, bid: bid)
    end
  end

  # def create(conn, %{"bid" => bid_params}) do
  #   case Auction.create_bid(bid_params) do
  #     {:ok, bid} ->
  #       conn
  #       |> put_flash(:info, "Bid created successfully.")
  #       |> redirect(to: Routes.item_path(conn, :show, bid.item_id))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "new.html", changeset: changeset)
  #   end
  # end
end
