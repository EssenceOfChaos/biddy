defmodule BiddyWeb.PageController do
  use BiddyWeb, :controller

  def index(conn, _params) do
    conn
    |> IO.inspect()
    |> render("index.html")
  end

  def protected(conn, _) do
    user = Guardian.Plug.current_resource(conn)

    conn
    |> put_flash(:info, "Flash message on protected resource!")
    |> render("protected.html", current_user: user)
  end
end
