defmodule BiddyWeb.Api.UserView do
  use BiddyWeb, :view

  def render("user.json", %{user: user}) do
    %{
      type: "user",
      id: user.id,
      username: user.display_name
    }
  end
end
