defmodule BiddyWeb.Api.BidView do
  use BiddyWeb, :view

  def render("bid.json", %{bid: bid}) do
    %{
      type: "bid",
      id: bid.id,
      amount: bid.amount,
      user: render_one(bid.user, BiddyWeb.Api.UserView, "user.json")
    }
  end
end
