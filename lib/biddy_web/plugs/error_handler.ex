defmodule BiddyWeb.Plugs.ErrorHandler do
  @moduledoc """
  Handles Guardian failure to authenticate.
  """
  use BiddyWeb, :controller

  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, _, _opts) do
    conn
    |> put_flash(:error, "Sorry, you must be signed in to access that feature!")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
