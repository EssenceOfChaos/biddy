defmodule BiddyWeb.Router do
  use BiddyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug Biddy.Accounts.GuardianPlugPipeline
  end

  # We use ensure_auth to fail if there is no one logged in
  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  scope "/", BiddyWeb do
    pipe_through [:browser, :auth]

    get "/", PageController, :index
    get "/logout", SessionController, :delete

    resources("/users", UserController, only: [:new, :create, :show, :index, :edit, :update])
    resources("/session", SessionController, only: [:create, :new])
    resources "/items", ItemController, only: [:index]
  end

  # Definitely logged in scope
  scope "/", BiddyWeb do
    pipe_through [:browser, :auth, :ensure_auth]

    get "/protected", PageController, :protected
    # resources("/users", UserController, only: [:delete, :edit, :update])
    # resources "/items", ItemController, only: [:new, :create, :edit, :update, :delete, :show]

    resources "/items", ItemController,
      only: [
        :index,
        :show,
        :new,
        :create,
        :edit,
        :update
      ] do
      resources "/bids", BidController, only: [:create]
    end

    resources "/users", UserController, only: [:show, :new, :create]
  end

  # Other scopes may use custom stacks.
  # scope "/api", BiddyWeb do
  #   pipe_through :api
  # end
end
