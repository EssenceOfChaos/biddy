defmodule BiddyWeb.ViewHelpers do
  @moduledoc """
  Global helper functions for the View Layer
  """

  use Timex

  def integer_to_currency(cents) do
    dollars_and_cents =
      cents
      |> Decimal.div(100)
      |> Decimal.round(2)

    "$#{dollars_and_cents}"
  end

  def formatted_datetime(datetime) do
    datetime
    |> Timex.format!("{YYYY}-{0M}-{0D} {h12}:{m}:{s}{am}")
  end

  def format_date(utc_datetime) do
    "#{utc_datetime.month}/#{utc_datetime.day}/#{utc_datetime.year}"
  end

  def get_highest_bid(bids) do
    bids
    |> Enum.max_by(fn x -> x.amount end)
  end
end
