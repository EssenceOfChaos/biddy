defmodule Biddy.Repo.Migrations.CreateUsers do
  @moduledoc """
  Migration for creating 'users' table
  """
  use Ecto.Migration

  def change do
    create table(:users) do
      add :display_name, :string
      add :avatar, :string
      add :email, :string, null: false
      add :password_hash, :string
      add :favorites, {:array, :string}

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
