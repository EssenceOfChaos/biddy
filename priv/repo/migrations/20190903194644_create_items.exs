defmodule Biddy.Repo.Migrations.CreateItems do
  @moduledoc """
  Migration for creating "items" table
  """
  use Ecto.Migration

  def change do
    create table(:items) do
      add :title, :string
      add :description, :string
      add :listed_at, :utc_datetime
      add :ends_at, :utc_datetime
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:items, [:user_id])
  end
end
