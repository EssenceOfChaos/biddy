defmodule Biddy.AuctionTest do
  use Biddy.DataCase

  alias Biddy.Auction

  describe "items" do
    alias Biddy.Auction.Item

    @valid_attrs %{description: "some description", ends_at: "2010-04-17T14:00:00Z", listed_at: "2010-04-17T14:00:00Z", title: "some title"}
    @update_attrs %{description: "some updated description", ends_at: "2011-05-18T15:01:01Z", listed_at: "2011-05-18T15:01:01Z", title: "some updated title"}
    @invalid_attrs %{description: nil, ends_at: nil, listed_at: nil, title: nil}

    def item_fixture(attrs \\ %{}) do
      {:ok, item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Auction.create_item()

      item
    end

    test "list_items/0 returns all items" do
      item = item_fixture()
      assert Auction.list_items() == [item]
    end

    test "get_item!/1 returns the item with given id" do
      item = item_fixture()
      assert Auction.get_item!(item.id) == item
    end

    test "create_item/1 with valid data creates a item" do
      assert {:ok, %Item{} = item} = Auction.create_item(@valid_attrs)
      assert item.description == "some description"
      assert item.ends_at == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert item.listed_at == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert item.title == "some title"
    end

    test "create_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Auction.create_item(@invalid_attrs)
    end

    test "update_item/2 with valid data updates the item" do
      item = item_fixture()
      assert {:ok, %Item{} = item} = Auction.update_item(item, @update_attrs)
      assert item.description == "some updated description"
      assert item.ends_at == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert item.listed_at == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert item.title == "some updated title"
    end

    test "update_item/2 with invalid data returns error changeset" do
      item = item_fixture()
      assert {:error, %Ecto.Changeset{}} = Auction.update_item(item, @invalid_attrs)
      assert item == Auction.get_item!(item.id)
    end

    test "delete_item/1 deletes the item" do
      item = item_fixture()
      assert {:ok, %Item{}} = Auction.delete_item(item)
      assert_raise Ecto.NoResultsError, fn -> Auction.get_item!(item.id) end
    end

    test "change_item/1 returns a item changeset" do
      item = item_fixture()
      assert %Ecto.Changeset{} = Auction.change_item(item)
    end
  end

  describe "bids" do
    alias Biddy.Auction.Bid

    @valid_attrs %{amount: 42}
    @update_attrs %{amount: 43}
    @invalid_attrs %{amount: nil}

    def bid_fixture(attrs \\ %{}) do
      {:ok, bid} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Auction.create_bid()

      bid
    end

    test "list_bids/0 returns all bids" do
      bid = bid_fixture()
      assert Auction.list_bids() == [bid]
    end

    test "get_bid!/1 returns the bid with given id" do
      bid = bid_fixture()
      assert Auction.get_bid!(bid.id) == bid
    end

    test "create_bid/1 with valid data creates a bid" do
      assert {:ok, %Bid{} = bid} = Auction.create_bid(@valid_attrs)
      assert bid.amount == 42
    end

    test "create_bid/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Auction.create_bid(@invalid_attrs)
    end

    test "update_bid/2 with valid data updates the bid" do
      bid = bid_fixture()
      assert {:ok, %Bid{} = bid} = Auction.update_bid(bid, @update_attrs)
      assert bid.amount == 43
    end

    test "update_bid/2 with invalid data returns error changeset" do
      bid = bid_fixture()
      assert {:error, %Ecto.Changeset{}} = Auction.update_bid(bid, @invalid_attrs)
      assert bid == Auction.get_bid!(bid.id)
    end

    test "delete_bid/1 deletes the bid" do
      bid = bid_fixture()
      assert {:ok, %Bid{}} = Auction.delete_bid(bid)
      assert_raise Ecto.NoResultsError, fn -> Auction.get_bid!(bid.id) end
    end

    test "change_bid/1 returns a bid changeset" do
      bid = bid_fixture()
      assert %Ecto.Changeset{} = Auction.change_bid(bid)
    end
  end
end
