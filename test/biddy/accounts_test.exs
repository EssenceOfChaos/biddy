defmodule Biddy.AccountsTest do
  @moduledoc """
  Testing the User model.
  """
  use Biddy.DataCase

  alias Biddy.Accounts

  describe "users" do
    alias Biddy.Accounts.User

    @valid_attrs %{
      avatar: "cooldude-img.png",
      display_name: "some_display_name",
      email: "cooldude1994@aol.com",
      password: "secretpassword"
    }
    @update_attrs %{
      avatar: "cooldude-img2.png",
      display_name: "display_name2",
      email: "lifesabeach89@hotmail.com",
      favorites: ["this", "that", "other"],
      password: "secretpassword2"
    }
    @invalid_attrs %{
      avatar: nil,
      display_name: nil,
      email: nil,
      favorites: nil,
      password_hash: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert {:ok, user} == Argon2.check_pass(user, "secretpassword", hash_key: :password)
      assert user.display_name == "some_display_name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert {:ok, user} == Argon2.check_pass(user, "secretpassword2", hash_key: :password)
      assert user.display_name == "display_name2"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end

# iex(3)> valid_attrs = %{
#   ...(3)>       avatar: "some avatar",
#   ...(3)>       display_name: "some display_name",
#   ...(3)>       email: "cooldude1994@aol.com",
#   ...(3)>       password: "some password"
#   ...(3)>     }

#   %{
#     avatar: "some avatar",
#     display_name: "some display_name",
#     email: "cooldude1994@aol.com",
#     password: "some password"
#   }

#   iex(4)> Accounts.create_user(valid_attrs)

#   [debug] QUERY OK db=73.9ms queue=32.8ms
#   INSERT INTO "users" ("avatar","display_name","email","inserted_at","updated_at") VALUES ($1,$2,$3,$4,$5) RETURNING "id" ["some avatar", "some display_name", "cooldude1994@aol.com", ~N[2019-09-02 03:16:18], ~N[2019-09-02 03:16:18]]

#   {:ok,
#    %Biddy.Accounts.User{
#      __meta__: #Ecto.Schema.Metadata<:loaded, "users">,
#      avatar: "some avatar",
#      display_name: "some display_name",
#      email: "cooldude1994@aol.com",
#      favorites: nil,
#      id: 1,
#      inserted_at: ~N[2019-09-02 03:16:18],
#      password: "$argon2id$v=19$m=131072,t=8,p=4$0iB3wx/6OZHzOYDTdVmyDA$/SnyHxHDbhub1OnlYdl9rMEimYsLwUxQw6F0X39D2FA",
#      password_confirmation: nil,
#      password_hash: nil,
#      updated_at: ~N[2019-09-02 03:16:18]
#    }}
