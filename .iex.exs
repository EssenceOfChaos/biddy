# Set aliases for developing in the `iex>` console.
alias Biddy.Repo
alias Biddy.Auction.{Item, Bid}
alias Biddy.Accounts.{User, Guardian}
