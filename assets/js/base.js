window.addEventListener('DOMContentLoaded', (event) => {
  console.log('DOM fully loaded and parsed');
  $('.message .close')
    .on('click', function () {
      $(this)
        .closest('.message')
        .transition('fade')
        ;
    })
    ;
    $('select.dropdown')
    .dropdown();
  let path = window.location.pathname;

  showActiveTab(path);

  if (path == "/session/new") {
    hideLoginButton();
  } else {
    showLoginButton();
  }
}); // end of addEventListener

// helper function to hide button
function hideLoginButton() {
  var button = document.getElementById("navbar_login");
  button.style.visibility = "hidden";
}
// helper function to show button
function showLoginButton() {
  var button = document.getElementById("navbar_login");
  button.style.visibility = "visible";
}

function showActiveTab(pathname) {
  if (pathname == "/items") {
    document.getElementById("items_tab").className = "active item";
  }
}
