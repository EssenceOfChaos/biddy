# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :biddy,
  ecto_repos: [Biddy.Repo]

# Configures the endpoint
config :biddy, BiddyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZLLNykd8XBIptILnq4A5WOJsfezIlREanluD0i7JD0+BoG/buEgBBStM/I601xvx",
  render_errors: [view: BiddyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Biddy.PubSub, adapter: Phoenix.PubSub.PG2]

# Configure Guardian
config :biddy, Biddy.Accounts.Guardian,
  issuer: "biddy",
  # secret_key: System.get_env("GUARDIAN_SK")
  secret_key: "gsSVctAYxhlaTn4oWmK5hx1By02tb6uCJVZumi6ujdTnVW0vEW2CSv28vacuZLnu"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
